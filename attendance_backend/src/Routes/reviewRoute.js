import { Router } from "express";
import {
  createReview,
  deleteReview,
  readReviewDetails,
  readAllReview,
  updateReview,
} from "../controller/reviewController.js";
let reviewRouter = Router();
reviewRouter
  .route("/") //lo.../reviews
  .post(createReview)
  .get(readAllReview);
reviewRouter
  .route("/:id")
  .get(readReviewDetails)
  .patch(updateReview)
  .delete(deleteReview);
export default reviewRouter;

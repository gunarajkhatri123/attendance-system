import { Router } from "express";


let firstRouter=Router()

firstRouter
.route("/admin")
.post((req,res)=>{
    res.json("i am admin method post")
})
.get((req,res)=>{
    res.json("i am admin method get")
})
.patch((req,res)=>{
    res.json("i am admin method patch")
})
.delete((req,res)=>{
    res.json("i am admin method delete")
})


firstRouter
.route("/admin/name")
.post((req,res)=>{
    res.json("i am admin name method post")
})
.get((req,res)=>{
    res.json("i am admin name method get")
})
.patch((req,res)=>{
    res.json("i am admin name method patch")
})
.delete((req,res)=>{
    res.json("i am admin name method delete")
})

export default firstRouter
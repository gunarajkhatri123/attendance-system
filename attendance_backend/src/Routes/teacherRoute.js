import { Router } from "express";
import {
  createTeacher,
  deleteTeacher,
  readTeacher,
  readTeacherDetails,
  updateTeacher,
} from "../controller/teacherController.js";

let TeacherRouter = Router();
TeacherRouter.route("/").post(createTeacher).get(readTeacher);
TeacherRouter.route("/:id")
  .get(readTeacherDetails)
  .patch(updateTeacher)
  .delete(deleteTeacher);
export default TeacherRouter;

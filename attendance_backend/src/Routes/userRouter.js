import { Router } from "express";
import { createUser, deleteUser, readUser, readUserDetails, updateUser } from "../controller/userController.js";

let userRouter = Router();
userRouter
  .route("/")
  .post(createUser)
  .get(readUser)
userRouter
  .route("/:id")
  .get(readUserDetails)
  .patch(updateUser)
  .delete(deleteUser)
export default userRouter;


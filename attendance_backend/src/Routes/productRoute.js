import { Router } from "express";
import {
  createProduct,
  deleteProduct,
  readProductDetails,
  readAllProduct,
  updateProduct,
} from "../controller/productController.js";
let productRouter = Router();
productRouter
  .route("/") //lo.../products
  .post(createProduct)
  .get(readAllProduct);
productRouter
  .route("/:id")
  .get(readProductDetails)
  .patch(updateProduct)
  .delete(deleteProduct);
export default productRouter;

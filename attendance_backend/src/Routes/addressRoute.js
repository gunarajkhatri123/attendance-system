import { Router } from "express";
import { createAddress, deleteAddress, readAddress, readAddressDetails, updateAddress } from "../controller/addressController.js";

let addressRouter = Router();
addressRouter
  .route("/")
  .post(createAddress)
  .get(readAddress)
addressRouter
  .route("/:id")
  .get(readAddressDetails)
  .patch(updateAddress)
  .delete(deleteAddress)
export default addressRouter;


import { Router } from "express";
import {
  createCourse,
  deleteCourse,
  readCourse,
  readCourseDetails,
  updateCourse,
} from "../controller/courseController.js";

let CourseRouter = Router();
CourseRouter.route("/").post(createCourse).get(readCourse);
CourseRouter.route("/:id")
  .get(readCourseDetails)
  .patch(updateCourse)
  .delete(deleteCourse);
export default CourseRouter;

import { Router } from "express";


let categoryRouter=Router()

categoryRouter
.route("/")
.post((req,res)=>{
    res.json("i am category post")
})
.get((req,res)=>{
    res.json("i am category get")
})
.patch((req,res)=>{
    res.json("i am category patch")
})
.delete((req,res)=>{
    res.json("i am category delete")
})

export default categoryRouter
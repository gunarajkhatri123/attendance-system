import expressAsyncHandler from "express-async-handler";
import { HttpStatus } from "../config/constant.js";
import errorResponse from "../helper/errorResponse.js";
import successResponse from "../helper/successResponse.js";
import { Address } from "../schema/model.js";

export let createAddress = expressAsyncHandler(async (req, res) => {
  let result = await Address.create(req.body);

  successResponse(
    res,
    HttpStatus.CREATED,
    "address created successfully",
    result
  );
});

export let readAddressDetails = expressAsyncHandler(async (req, res, next) => {
  let result = await Address.findById(req.params.id);
  successResponse(
    res,
    HttpStatus.CREATED,
    "read address details successful",
    result
  );
});

export let readAddress = expressAsyncHandler(async (req, res, next) => {
  let result = await Address.find();
  successResponse(res, HttpStatus.CREATED, "address read successful", result);
});

export let updateAddress = expressAsyncHandler(async (req, res, next) => {
  let result = await Address.findByIdAndUpdate(req.params.id, req.body);
  successResponse(
    res,
    HttpStatus.CREATED,
    "address updated successfully",
    result
  );
});

export let deleteAddress = expressAsyncHandler(async (req, res, next) => {
  let result = await Address.findByIdAndDelete(req.params.id);
  successResponse(res, HttpStatus.OK, "address deleted successfully", result);
});

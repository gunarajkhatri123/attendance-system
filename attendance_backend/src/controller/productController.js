import { HttpStatus } from "../config/constant.js";
import errorResponse from "../helper/errorResponse.js";
import successResponse from "../helper/successResponse.js";
import { Product } from "../schema/model.js";
import expressAsyncHandler from "express-async-handler";
export let createProduct = expressAsyncHandler(async (req, res, next) => {
  let result = await Product.create(req.body);

  successResponse(
    res,
    HttpStatus.CREATED,
    "Product created successfully",
    result
  );
});

export let readProductDetails = expressAsyncHandler(async (req, res, next) => {
  let result = await Product.findById(req.params.id);
  successResponse(
    res,
    HttpStatus.OK,
    "Read Product details successfully",
    result
  );
});

export let readAllProduct = expressAsyncHandler(async (req, res, next) => {
  let result = await Product.find({});
  successResponse(res, HttpStatus.OK, "Read Product  successfully", result);
});

export let deleteProduct = expressAsyncHandler(async (req, res, next) => {
  let result = await Product.findByIdAndDelete(req.params.id);
  successResponse(res, HttpStatus.OK, "Delete Product  successfully.", result);
});

export let updateProduct = expressAsyncHandler(async (req, res, next) => {
  let result = await Product.findByIdAndUpdate(req.params.id, req.body);
  successResponse(
    res,
    HttpStatus.CREATED,
    "Update Product  successfully.",
    result
  );
});

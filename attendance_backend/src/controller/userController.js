import expressAsyncHandler from "express-async-handler";
import { HttpStatus } from "../config/constant.js";
import errorResponse from "../helper/errorResponse.js";
import successResponse from "../helper/successResponse.js";
import User from "../schema/model.js";

export let createUser = expressAsyncHandler(async (req, res) => {
  let result = await User.create(req.body);
  successResponse(res, HttpStatus.CREATED, "user created successfully", result);
});

export let readUserDetails = expressAsyncHandler(async (req, res, next) => {
  let result = await User.findById(req.params.id);
  successResponse(
    res,
    HttpStatus.CREATED,
    "read user details successful",
    result
  );
});

export let readUser = expressAsyncHandler(async (req, res, next) => {
  let result = await User.find();
  //   let result = await User.find({ name: "nitan"}); // this is exact searching
  //   let result = await User.find({ name: "nitan", age: 29 });
  //   let result = await User.find({ age: { $gt: 33 } });
  //   let result = await User.find({ age: { $gte: 33 } });
  //   let result = await User.find({ age: { $lt: 33 } });
  //   let result = await User.find({ age: { $lte: 33 } });
  //   let result = await User.find({ age: { $ne: 33 } });  //not equal
  //   let result = await User.find({ name: { $in: ["nitan", "zzz"] } }); // in means include
  //   let result = await User.find({
  //     $or: [{ name: "nitan", age: 33 }, { name: "zzz" }],
  //   });
  //   let result = await User.find({
  //     $and: [{ age: 33 }, { name: "zzz" }],
  //   });
  //   let result = await User.find({ "location.country": "china" }); //always wrap key by double quotes
  // let result = await User.find({}).select("name email age -_id"); //-_id hides id (-)hides dont use - with others

  successResponse(res, HttpStatus.CREATED, "user read successful", result);
});

export let updateUser = expressAsyncHandler(async (req, res, next) => {
  let result = await User.findByIdAndUpdate(req.params.id, req.body);
  successResponse(res, HttpStatus.CREATED, "user updated successfully", result);
});

export let deleteUser = expressAsyncHandler(async (req, res, next) => {
  let result = await User.findByIdAndDelete(req.params.id);
  successResponse(res, HttpStatus.OK, "user deleted successfully", result);
});

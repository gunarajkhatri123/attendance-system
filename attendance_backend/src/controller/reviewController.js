import { HttpStatus } from "../config/constant.js";
import errorResponse from "../helper/errorResponse.js";
import successResponse from "../helper/successResponse.js";
import { Review } from "../schema/model.js";
import expressAsyncHandler from "express-async-handler";
export let createReview = expressAsyncHandler(async (req, res, next) => {
  let result = await Review.create(req.body);
  successResponse(
    res,
    HttpStatus.CREATED,
    "Review created successfully",
    result
  );
});
export let readReviewDetails = expressAsyncHandler(async (req, res, next) => {
  let result = await Review.findById(req.params.id);
  successResponse(
    res,
    HttpStatus.OK,
    "Read Review details successfully",
    result
  );
});
export let readAllReview = expressAsyncHandler(async (req, res, next) => {
  let result = await Review.find({})
    .populate("userId", "name age")
    .populate("productId", "name price quantity");
  successResponse(res, HttpStatus.OK, "Read Review  successfully", result);
});
export let deleteReview = expressAsyncHandler(async (req, res, next) => {
  let result = await Review.findByIdAndDelete(req.params.id);
  successResponse(res, HttpStatus.OK, "Delete Review  successfully.", result);
});
export let updateReview = expressAsyncHandler(async (req, res, next) => {
  let result = await Review.findByIdAndUpdate(req.params.id, req.body);
  successResponse(
    res,
    HttpStatus.CREATED,
    "Update Review  successfully.",
    result
  );
});

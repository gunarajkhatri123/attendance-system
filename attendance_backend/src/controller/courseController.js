import expressAsyncHandler from "express-async-handler";
import { HttpStatus } from "../config/constant.js";
import errorResponse from "../helper/errorResponse.js";
import successResponse from "../helper/successResponse.js";
import { Course } from "../schema/model.js";

export let createCourse = expressAsyncHandler(async (req, res) => {
  let result = await Course.create(req.body);
  successResponse(
    res,
    HttpStatus.CREATED,
    "Course created successfully",
    result
  );
});

export let readCourseDetails = expressAsyncHandler(async (req, res, next) => {
  let result = await Course.findById(req.params.id);
  successResponse(
    res,
    HttpStatus.CREATED,
    "read Course details successful",
    result
  );
});

export let readCourse = expressAsyncHandler(async (req, res, next) => {
  let result = await Course.find({}).populate("course");
  //   let result = await Course.find({ name: "nitan"}); // this is exact searching
  //   let result = await Course.find({ name: "nitan", age: 29 });
  //   let result = await Course.find({ age: { $gt: 33 } });
  //   let result = await Course.find({ age: { $gte: 33 } });
  //   let result = await Course.find({ age: { $lt: 33 } });
  //   let result = await Course.find({ age: { $lte: 33 } });
  //   let result = await Course.find({ age: { $ne: 33 } });  //not equal
  //   let result = await Course.find({ name: { $in: ["nitan", "zzz"] } }); // in means include
  //   let result = await Course.find({
  //     $or: [{ name: "nitan", age: 33 }, { name: "zzz" }],
  //   });
  //   let result = await Course.find({
  //     $and: [{ age: 33 }, { name: "zzz" }],
  //   });
  //   let result = await Course.find({ "location.country": "china" }); //always wrap key by double quotes
  // let result = await Course.find({}).select("name email age -_id"); //-_id hides id (-)hides dont use - with others

  successResponse(res, HttpStatus.CREATED, "Course read successful", result);
});

export let updateCourse = expressAsyncHandler(async (req, res, next) => {
  let result = await Course.findByIdAndUpdate(req.params.id, req.body);
  successResponse(
    res,
    HttpStatus.CREATED,
    "Course updated successfully",
    result
  );
});

export let deleteCourse = expressAsyncHandler(async (req, res, next) => {
  let result = await Course.findByIdAndDelete(req.params.id);
  successResponse(res, HttpStatus.OK, "Course deleted successfully", result);
});

import expressAsyncHandler from "express-async-handler";
import { HttpStatus } from "../config/constant.js";
import errorResponse from "../helper/errorResponse.js";
import successResponse from "../helper/successResponse.js";
import { Teacher } from "../schema/model.js";

export let createTeacher = expressAsyncHandler(async (req, res) => {
  let result = await Teacher.create(req.body);
  successResponse(
    res,
    HttpStatus.CREATED,
    "Teacher created successfully",
    result
  );
});

export let readTeacherDetails = expressAsyncHandler(async (req, res, next) => {
  let result = await Teacher.findById(req.params.id);
  successResponse(
    res,
    HttpStatus.CREATED,
    "read Teacher details successful",
    result
  );
});

export let readTeacher = expressAsyncHandler(async (req, res, next) => {
  let result = await Teacher.find().populate("course");
  //   let result = await Teacher.find({ name: "nitan"}); // this is exact searching
  //   let result = await Teacher.find({ name: "nitan", age: 29 });
  //   let result = await Teacher.find({ age: { $gt: 33 } });
  //   let result = await Teacher.find({ age: { $gte: 33 } });
  //   let result = await Teacher.find({ age: { $lt: 33 } });
  //   let result = await Teacher.find({ age: { $lte: 33 } });
  //   let result = await Teacher.find({ age: { $ne: 33 } });  //not equal
  //   let result = await Teacher.find({ name: { $in: ["nitan", "zzz"] } }); // in means include
  //   let result = await Teacher.find({
  //     $or: [{ name: "nitan", age: 33 }, { name: "zzz" }],
  //   });
  //   let result = await Teacher.find({
  //     $and: [{ age: 33 }, { name: "zzz" }],
  //   });
  //   let result = await Teacher.find({ "location.country": "china" }); //always wrap key by double quotes
  // let result = await Teacher.find({}).select("name email age -_id"); //-_id hides id (-)hides dont use - with others

  successResponse(res, HttpStatus.CREATED, "Teacher read successful", result);
});

export let updateTeacher = expressAsyncHandler(async (req, res, next) => {
  let result = await Teacher.findByIdAndUpdate(req.params.id, req.body);
  successResponse(
    res,
    HttpStatus.CREATED,
    "Teacher updated successfully",
    result
  );
});

export let deleteTeacher = expressAsyncHandler(async (req, res, next) => {
  let result = await Teacher.findByIdAndDelete(req.params.id);
  successResponse(res, HttpStatus.OK, "Teacher deleted successfully", result);
});

import { Schema } from "mongoose";

let addressSchema=Schema({
    city:{
        type:String,
    },
    province:{
        type:Number,
    },
    exactLocation:{
        type:String,
    }
})

export default addressSchema
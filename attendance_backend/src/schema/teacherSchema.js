import { Schema } from "mongoose";

let teacherSchema = Schema({
  name: {
    type: String,
    // default: "ram",
    // lowercase: true,
    // uppercase: true,
    trim: true,
    required: [true, "name field is required"],
    // minLength: [3, "name must be at least 3 characters long"],
    // maxLength: [20, "name must be at most 20 characters"],
    validate: (value) => {
      let isMatch = /^[A-Za-z0-9 _-]+$/.test(value);
      if (!isMatch) {
        throw new Error("alphabet number and spaces are only supported");
      }
    },
  },

  phoneNumber: {
    type: Number,
    // min: [1000000000, "phoneNumber must be of 10 digit"],
    // max: [9999999999, "phoneNumber must be of 10 digit"],
    validate: (value) => {
      let strNumber = String(value); //+ makes number
      if (strNumber.length !== 10) {
        throw new Error("phoneNumber must be of 10 digit");
      }
    },
  },

  email: {
    type: String,
    validate: (value) => {
      let isEmail =
        /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(
          value
        );
      if (!isEmail) {
        throw new Error("@ is required");
      }
    },
  },
  password: {
    type: String,
    validate: (value) => {
      let isPassword =
        /^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[!@#$%^&*()_+-={}|[\]:;'\",.<>?/]).{8,}$/.test(
          value
        );
      if (!isPassword) {
        throw new Error(
          "A password contains at least eight characters, including at least one number and includes both lower and uppercase letters and special characters, for example #, ?, !."
        );
      }
    },
  },
  gender: {
    type: String,
    enum: {
      values: ["male", "female", "other"],
      message: (notEnum) => {
        return `${notEnum.value} is not valid enum`;
      },
    },
  },
  course: {
    type: [Schema.ObjectId],
    ref: "Course",
  },
});

export default teacherSchema;

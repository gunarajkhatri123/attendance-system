import { Schema } from "mongoose";
export let reviewSchema = Schema({
  rating: {
    type: Number,
  },
  comments: {
    type: String,
  },
  userId: {
    type: Schema.ObjectId,
    ref: "User",
  },
  productId: {
    type: Schema.ObjectId,
    ref: "Product",
  },
});

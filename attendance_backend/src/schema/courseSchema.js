import { Schema } from "mongoose";

let courseSchema = Schema({
  name: {
    type: String,
    // default: "ram",
    // lowercase: true,
    // uppercase: true,
    trim: true,
    required: [true, "name field is required"],
    // minLength: [3, "name must be at least 3 characters long"],
    // maxLength: [20, "name must be at most 20 characters"],
    validate: (value) => {
      let isMatch = /^[A-Za-z0-9 _-]+$/.test(value);
      if (!isMatch) {
        throw new Error("alphabet number and spaces are only supported");
      }
    },
  },
});

export default courseSchema;

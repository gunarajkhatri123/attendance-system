import { model } from "mongoose";
import userSchema from "./userScheme.js";
import addressSchema from "./addressSchema.js";
import { productSchema } from "./productSchema.js";
import { reviewSchema } from "./reviewSchema.js";
import courseSchema from "./courseSchema.js";
import teacherSchema from "./teacherSchema.js";

let User = model("User", userSchema);
export let Address = model("Address", addressSchema);
export let Product = model("Product", productSchema);
export let Review = model("Review", reviewSchema);
export let Teacher = model("Teacher", teacherSchema);
export let Course = model("Course", courseSchema);

export default User;

import express, { json } from "express";
import firstRouter from "./src/Routes/firstRoute.js";
import productRouter from "./src/Routes/productRoute.js";
import categoryRouter from "./src/Routes/categoryRoute.js";
import connectDb from "./src/connectdb/connectdb.js";
import User from "./src/schema/model.js";
import userRouter from "./src/Routes/userRouter.js";
import errorMiddleware from "./src/middleware/errorMiddleware.js";
import addressRouter from "./src/Routes/addressRoute.js";
import expressAsyncHandler from "express-async-handler";
import reviewRouter from "./src/Routes/reviewRoute.js";
import TeacherRouter from "./src/Routes/teacherRoute.js";
import CourseRouter from "./src/Routes/courseRoute.js";

let app = express();
let port = 8000;

// app.use(bodyParser.json())
app.use(json());

// app.use((req,res,next)=>{
//     console.log("i am first")
//     next("abcd")},
//     (req,res,next)=>{
//         console.log("i am fi")
//         next()})

// app.use((req,res,next)=>{
//     console.log("i am second")
//     next()})

app.use("/", firstRouter);
app.use("/products", productRouter);
app.use("categories", categoryRouter);
app.use("/users", userRouter);
app.use("/addresses", addressRouter);
app.use("/reviews", reviewRouter);
app.use("/teacher", TeacherRouter);
app.use("/course", CourseRouter);

// app.use((req,res,next)=>{
//     console.log("i am thirddddd")
//     })

// app.use((err,req,res,next)=>{
//     console.log("i am error middleware")
//     res.status(HttpStatus.OK).json("error has occurred")
//     next("gh")
// })
// app.use((err,req,res,next)=>{
//     console.log("i am error 2 middleware")
// })

// define the content of array=>model

// let CreateUser=async()=>{
//     let data={
//         name: "manish",
//         age:"21",
//         isMarried:false
//     }
//     try {
//         let result = await User.create(data)
//         console.log(result)
//     } catch (error) {
//         console.log(error.message)
//     }
// }
// CreateUser()

connectDb();

app.use(errorMiddleware);

app.listen(port, () => {
  console.log(`app is listening at port ${port}`);
});
